/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.peopleproject;

/**
 *
 * @author ACER
 */
public class FirstChild extends People{
    public FirstChild(String name, String hair){
        super(name,hair,15);
        System.out.println("FirstChild Created");
    }
    @Override
    public void age(){
        System.out.println(name + " Age is " + numberOfAge + " Years");
    }
    @Override
     public void call(){
        System.out.println(name + " call > Hi,Ferb&Phineas!!!");
    }
}
