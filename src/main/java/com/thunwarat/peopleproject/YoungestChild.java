/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.peopleproject;

/**
 *
 * @author ACER
 */
public class YoungestChild extends People{
    public YoungestChild(String name, String hair){
        super(name,hair,8);
        System.out.println("YoungestChild Created");
    }
    @Override
    public void age(){
        System.out.println(name + " Age is " + numberOfAge + " Years");
    }
    @Override
    public void call(){
       System.out.println(name + " call > Hi,Candace&Ferb!!!");
    }
}
