/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.peopleproject;

/**
 *
 * @author ACER
 */
public class People {
    protected String name;
    protected int numberOfAge = 0;
    protected String hair;
    public People(String name, String hair, int numberOfAge){
        System.out.println("People Created");
        this.name = name;
        this.hair = hair;
        this.numberOfAge = numberOfAge;
    }
    
    public void age(){
        System.out.println("People Age");
    }
    
    public void call(){
        System.out.println("People Call");
        System.out.println("name is " + this.name + " hair is " + this.hair + " Age is " + this.numberOfAge);
    }

    public String getName() {
        return name;
    }

    public int getNumberOfAge() {
        return numberOfAge;
    }

    public String getHair() {
        return hair;
    }
    
}
