/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.peopleproject;

/**
 *
 * @author ACER
 */
public class TestPeople {
    public static void main(String[] args){
        People people = new People("Peo", "Black",0);
        people.call();
        people.age();
        
        FirstChild candace = new FirstChild("Candace", "Orange");
        candace.call();
        candace.age();
        System.out.println("--------------------------");
        
        MiddleChild ferb = new MiddleChild("Ferb", "Green");
        ferb.call();
        ferb.age();
        ferb.skillguitar();
         System.out.println("--------------------------");
        
        YoungestChild phineas = new YoungestChild("Phineas", "Red");
        phineas.call();
        phineas.age();
         System.out.println("--------------------------");
        
    }
}
